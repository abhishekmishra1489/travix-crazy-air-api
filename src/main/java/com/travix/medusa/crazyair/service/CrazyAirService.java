package com.travix.medusa.crazyair.service;

import com.travix.medusa.crazyair.model.CrazyAirRequest;
import com.travix.medusa.crazyair.model.CrazyAirRequestList;
import com.travix.medusa.crazyair.model.CrazyAirResponse;
import com.travix.medusa.crazyair.model.CrazyAirResponseList;
import com.travix.medusa.crazyair.model.enums.AirportCodeEnum;
import com.travix.medusa.crazyair.model.enums.CabinClassEnum;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

@Service
public class CrazyAirService {


    public CrazyAirResponseList retrieveFlightDetails(CrazyAirRequest crazyAirRequest) {

        CrazyAirResponse crazyAirResponse1 =
                CrazyAirResponse.builder()
                        .airline("AirEurope")
                        .price(196)
                        .cabinClass(CabinClassEnum.B)
                        .departureAirportCode(crazyAirRequest.getOrigin())
                        .destinationAirportCode(crazyAirRequest.getDestination())
                        .departureDate(crazyAirRequest.getDepartureDate())
                        .arrivalDate(crazyAirRequest.getReturnDate())
                        .build();
        CrazyAirResponse crazyAirResponse2 =
                CrazyAirResponse.builder()
                        .airline("SAS")
                        .price(207)
                        .cabinClass(CabinClassEnum.B)
                        .departureAirportCode(crazyAirRequest.getOrigin())
                        .destinationAirportCode(crazyAirRequest.getDestination())
                        .departureDate(crazyAirRequest.getDepartureDate())
                        .arrivalDate(crazyAirRequest.getReturnDate())
                        .build();
        CrazyAirResponse crazyAirResponse3 =
                CrazyAirResponse.builder()
                        .airline("British Airways")
                        .price(400)
                        .cabinClass(CabinClassEnum.B)
                        .departureAirportCode(crazyAirRequest.getOrigin())
                        .destinationAirportCode(crazyAirRequest.getDestination())
                        .departureDate(crazyAirRequest.getDepartureDate())
                        .arrivalDate(crazyAirRequest.getReturnDate())
                        .build();
        CrazyAirResponse crazyAirResponse4 =
                CrazyAirResponse.builder()
                        .airline("Alitalia")
                        .price(202)
                        .cabinClass(CabinClassEnum.E)
                        .departureAirportCode(crazyAirRequest.getOrigin())
                        .destinationAirportCode(crazyAirRequest.getDestination())
                        .departureDate(crazyAirRequest.getDepartureDate())
                        .arrivalDate(crazyAirRequest.getReturnDate())
                        .build();



        return CrazyAirResponseList.builder().
                crazyAirResponseList(Collections.unmodifiableList(Arrays.asList(crazyAirResponse1, crazyAirResponse2, crazyAirResponse3 , crazyAirResponse4)))
                .build();


    }
}
