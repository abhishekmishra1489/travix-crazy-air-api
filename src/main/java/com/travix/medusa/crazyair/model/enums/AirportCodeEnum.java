package com.travix.medusa.crazyair.model.enums;

public enum AirportCodeEnum {
    LHR("LONDON"),
    AMS("AMSTERDAM"),
    MUC("MUCNICH"),
    ZSH("ZURICH"),
    NCE("NICE");

    String value;

    AirportCodeEnum(String value) {
        this.value = value;
    }

}
