package com.travix.medusa.crazyair.model.enums;

public enum CabinClassEnum {
    E("ECONOMY CLASS"),
    B("B");

    String value;

    CabinClassEnum(String value) {
        this.value = value;
    }

}
