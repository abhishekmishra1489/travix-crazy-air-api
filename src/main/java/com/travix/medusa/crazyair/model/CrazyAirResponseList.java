package com.travix.medusa.crazyair.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CrazyAirResponseList {
    private List<CrazyAirResponse> crazyAirResponseList ;
}
