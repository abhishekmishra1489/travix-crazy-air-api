package com.travix.medusa.crazyair;

import brave.sampler.Sampler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class CrazyAirApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrazyAirApiApplication.class, args);
	}

	/**
	 * This sleuth log data follows the format of:
	 * [application name, traceId, spanId, export]
	 *
	 * Application name – This is the name we set in the properties file and can be used to aggregate logs from multiple instances of the same application.
	 * TraceId – This is an id that is assigned to a single request, job, or action. Something like each unique user initiated web request will have its own traceId.
	 * SpanId – Tracks a unit of work. Think of a request that consists of multiple steps. Each step could have its own spanId and be tracked individually. By default, any application flow will start with same TraceId and SpanId.
	 * Export – This property is a boolean that indicates whether or not this log was exported to an aggregator like Zipkin , which plays an important role in analyzing logs created by Sleuth.
	 */
	@Bean
	public Sampler defaultSampler() {
		return Sampler.ALWAYS_SAMPLE;
	}

}
