package com.travix.medusa.crazyair.controller;

import com.google.gson.Gson;
import com.netflix.ribbon.proxy.annotation.Http;
import com.travix.medusa.crazyair.model.CrazyAirRequest;
import com.travix.medusa.crazyair.model.CrazyAirRequestList;
import com.travix.medusa.crazyair.model.CrazyAirResponseList;
import com.travix.medusa.crazyair.service.CrazyAirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/crazy-air")
public class CrazyAirController {

    private CrazyAirService crazyAirService;

    @Autowired
    public CrazyAirController(CrazyAirService crazyAirService) {
        this.crazyAirService = crazyAirService;
    }

    @PostMapping("/flights-detail")
    public ResponseEntity<?> getCrazyAirFlightDetails(@RequestBody CrazyAirRequest crazyAirRequest) {
        CrazyAirResponseList crazyAirResponseList = crazyAirService.retrieveFlightDetails(crazyAirRequest);
        return new ResponseEntity<>(crazyAirResponseList, HttpStatus.OK);
    }



}
