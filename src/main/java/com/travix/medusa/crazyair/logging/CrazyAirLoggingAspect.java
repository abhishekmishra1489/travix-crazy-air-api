package com.travix.medusa.crazyair.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/***
 * Logging Aspect will help to log every method of the respective class invoked in each journey .
 * Ideally the  logger  should be in * debug * mode when moving to production , but for convinience to check logs its left as * info * for time being  .
 *
 */

@Aspect
@Component
@Slf4j
public class CrazyAirLoggingAspect {

    @Pointcut("(within(@org.springframework.stereotype.Component *) || " +
            "within(@org.springframework.stereotype.Service *) || " +
            "within(@org.springframework.web.bind.annotation.RestController *)) && " +
            "within(com.travix.medusa.crazyair..*)")

    public void beanAnnotatedWithServiceAnnotation() {
        // just needs this method to be invoked no implementation required
    }

    @Around("beanAnnotatedWithServiceAnnotation()")
    public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String methodName = signature.getName();
        Object[] objects = joinPoint.getArgs();
        String arguments = Arrays.toString(objects);

        logger.info("Enter: {}.{}() with argument[s] = {}", declaringTypeName,
                methodName, arguments);

        Object result = joinPoint.proceed();
        logger.info("Exit: {}.{}() with result = {}", declaringTypeName,
                methodName, result);

        return result;

    }
}